#language:ru
@test
  Функционал: Тестирование сервиса HelpDesk API
    Сценарий: Создание тикета с высоким приоритетом
      И сгенерировать переменные
        | id              | 0                          |
        | due_date        | 2022-02-24                 |
        | title           | EEEEEE_EEEE                |
        | created         | 2022-03-16T10:39:53.813174 |
        | modified        | 2022-03-16T10:39:53.813178 |
        | submitter_email | EEEEE@mail.ru              |
        | status          | 4                          |
        | on_hold         | true                       |
        | description     | EEEEEEEEEEE                |
        | resolution      | EEEEEEEEEEE                |
        | priority        | 2                          |
        | secret_key      | EEEEEEEEEE                 |
        | queue           | 1                          |

      И создать запрос
        | method  | url                                               | body              |
        | POST    | https://at-sandbox.workbench.lanit.ru/api/tickets | createTicket.json |
      И добавить header
        | Content-Type | application/json |
        | admin        | adminat          |
      Тогда отправить запрос
      И статус код 201
      И извлечь данные
      | id | $.id |

      И создать запрос
      | method | url                                                     |
      | GET    | https://at-sandbox.workbench.lanit.ru/api/tickets/${id} |
    И добавить header
      | Authorization | token c1803e6e3b12359683796496625e8518b2e082c3   |

    Тогда отправить запрос
    И статус код 200

