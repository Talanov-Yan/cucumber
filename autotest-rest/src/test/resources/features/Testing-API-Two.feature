#language:ru
@test
Функционал: Тестирование сервиса HelpDesk API
  Сценарий:  Негативная проверка: перевода статуса тикета из "Закрыт" в "Открыт"

    И сгенерировать переменные
      | id              | 0                          |
      | due_date        | 2022-02-24                 |
      | title           | EEEEEE_EEEE                |
      | created         | 2022-03-16T10:39:53.813174 |
      | modified        | 2022-03-16T10:39:53.813178 |
      | submitter_email | EEEEE@mail.ru              |
      | status          | 4                          |
      | on_hold         | true                       |
      | description     | EEEEEEEEEEE                |
      | resolution      | EEEEEEEEEEE                |
      | priority        | 2                          |
      | secret_key      | EEEEEEEEEE                 |
      | queue           | 1                          |

    И создать запрос
      | method  | url                                               | body              |
      | POST    | https://at-sandbox.workbench.lanit.ru/api/tickets | createTicket.json |
    И добавить header
      | Content-Type | application/json |
      | admin        | adminat          |
    Тогда отправить запрос
    И статус код 201
    И извлечь данные
      | id              | $.id              |
      | due_date        | $.due_date        |
      | title           | $.title           |
      | created         | $.created         |
      | modified        | $.modified        |
      | submitter_email | $.submitter_email |
      | status          | $.status          |
      | on_hold         | $.on_hold         |
      | description     | $.description     |
      | resolution      | $.resolution      |
      | priority        | $.priority        |
      | secret_key      | $.secret_key      |
      | queue           | $.queue           |

    И сгенерировать переменные
        | username | admin   |
        | password | adminat |
      И создать запрос
        | method   | url                                             | body             |
        | POST     | https://at-sandbox.workbench.lanit.ru/api/login | createLogin.json |
      И добавить header
        | Content-Type  | application/json |
        | admin         | adminat          |
      И отправить запрос
      И статус код 200
      И извлечь данные
        | token | $.token |

    И сгенерировать переменные
      | id              | ${id}              |
      | due_date        | ${due_date}        |
      | title           | ${title}           |
      | created         | ${created}         |
      | modified        | ${modified}        |
      | submitter_email | ${submitter_email} |
      | status          | 2                  |
      | on_hold         | ${on_hold}         |
      | description     | ${description}     |
      | resolution      | ${resolution}      |
      | priority        | ${priority}        |
      | secret_key      | ${secret_key}      |
      | queue           | ${queue}           |
    И создать запрос
      | method  | url                                                     | body              |
      | PUT     | https://at-sandbox.workbench.lanit.ru/api/tickets/${id} | createTicket.json |
    И добавить header
      | Authorization | token c1803e6e3b12359683796496625e8518b2e082c3 |
      | Content-Type  | application/json                               |
    Тогда отправить запрос
    И статус код 422

#    И создать запрос
#      | method | url                                         |
#      | GET    | https://at-sandbox.workbench.lanit.ru/api/tickets/${id} |
#    И добавить header
#      | Authorization | token c1803e6e3b12359683796496625e8518b2e082c3 |
#
#    Тогда отправить запрос
#    И статус код 200