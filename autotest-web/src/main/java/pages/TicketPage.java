package pages;

import com.codeborne.selenide.SelenideElement;
import ru.lanit.at.web.annotations.Name;
import ru.lanit.at.web.pagecontext.WebPage;

import static com.codeborne.selenide.Selenide.$x;

@Name(value = "TicketPage")
public class TicketPage extends WebPage {
    @Name("AttachFileBtn")
    private SelenideElement attachFileBtn = $x("//button[@id=\"ShowFileUpload\"]");

    @Name("BrowseInput")
    private SelenideElement browseInput = $x("//input[@id = \"file0\"]");

    @Name("BrowseLabel")
    private SelenideElement browseLabel = $x("//input[@id = \"file0\"]/..");

    @Name("UpdateThisTicket")
    private SelenideElement updateBtn = $x("//button[text()=\"Update This Ticket\"]");
}
