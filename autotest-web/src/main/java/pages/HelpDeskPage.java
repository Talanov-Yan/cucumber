package pages;

import com.codeborne.selenide.SelenideElement;
import ru.lanit.at.web.annotations.Name;
import ru.lanit.at.web.pagecontext.WebPage;

import static com.codeborne.selenide.Selenide.$x;

@Name(value = "HelpDesk")
public class HelpDeskPage extends WebPage {

    @Name("Кнопка для перехода на страницу авторизации")
    private SelenideElement loginBtnPage = $x("//a[@id=\"userDropdown\"]");

    @Name("Переход на страницу создания тикета")
    private SelenideElement newTicketBtnPage = $x("//span[text()=\"New Ticket\"]/..");
    @Name("Переход на страницу с тикетами")
    private SelenideElement allTicketBtnPage = $x("//span[text()=\"All Tickets\"]");

}
