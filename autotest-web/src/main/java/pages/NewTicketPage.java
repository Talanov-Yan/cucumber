package pages;

import com.codeborne.selenide.SelenideElement;
import ru.lanit.at.web.annotations.Name;
import ru.lanit.at.web.pagecontext.WebPage;

import static com.codeborne.selenide.Selenide.$x;

@Name(value = "NewTicketPage")
public class NewTicketPage extends WebPage {
    @Name("queue")
    private SelenideElement queue = $x("//select[@id='id_queue']");
    @Name("Some Product")
    private SelenideElement someProduct = $x("//option[text() = \"Some Product\"]");

    @Name("title")
    private SelenideElement title = $x("//input[@id='id_title']");

    @Name("description")
    private SelenideElement description = $x("//textarea[@id='id_body']");

    @Name("priority")
    private SelenideElement priority = $x("//select[@id='id_priority']");
    @Name("High")
    private SelenideElement high = $x("//option[text() = \"2. High\"]");

    @Name("email")
    private SelenideElement email = $x("//input[@id='id_submitter_email']");

    @Name("submitTicketBtn")
    private SelenideElement submitTicketBtn = $x("(//button[@type='submit'])[2]");

}
