package pages;

import com.codeborne.selenide.SelenideElement;
import ru.lanit.at.web.annotations.Name;
import ru.lanit.at.web.pagecontext.WebPage;

import static com.codeborne.selenide.Selenide.$x;

@Name(value = "LoginPage")
public class HelpDeskLoginPage extends WebPage {

    @Name("Поле логина")
    private SelenideElement userFld = $x("//input[@id='username']");

    @Name("Поле пароля")
    private SelenideElement passwordFld = $x("//input[@id='password']");

    @Name("Кнопка авторизации")
    private SelenideElement loginBtn = $x("//input[@value='Login']");


}
