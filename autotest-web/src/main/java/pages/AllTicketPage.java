package pages;

import com.codeborne.selenide.SelenideElement;
import ru.lanit.at.web.annotations.Name;
import ru.lanit.at.web.pagecontext.WebPage;

import static com.codeborne.selenide.Selenide.$x;

@Name(value = "AllTicket")
public class AllTicketPage extends WebPage {

    @Name("SaveQuery")
    private SelenideElement saveQuery = $x("//*[@id=\"headingTwo\"]/h5/button");

    @Name("QueryName")
    private SelenideElement queryName = $x("//input[@id=\"id_title\"]");

    @Name("SaveQueryBtn")
    private SelenideElement saveQueryBtn = $x("//input[@value=\"Save Query\"]");
}
