#language:ru
@test1
  Функционал: Проверка HelpDesk

    Сценарий: Редактирование созданного тикета (с прикреплением файла в созданный тикет)

      Если открыть url "https://at-sandbox.workbench.lanit.ru/"
      Когда инициализация страницы "HelpDesk"

      Если кликнуть на элемент "Кнопка для перехода на страницу авторизации"
      И переход на страницу "LoginPage"
      Когда ввести в поле "Поле логина" значение "admin"
      Когда ввести в поле "Поле пароля" значение "adminat"
      Если кликнуть на элемент "Кнопка авторизации"

      И переход на страницу "HelpDesk"

      Если кликнуть на элемент "Переход на страницу создания тикета"
      И переход на страницу "NewTicketPage"
      Если кликнуть на элемент "queue"
      Когда подождать появления элемента "Some Product" в течение 2 секунд
      Если кликнуть на элемент по тексту "Some Product"
      Когда ввести в поле "title" значение "Название проблемы"
      Когда ввести в поле "description" значение "Описание проблемы"
      Если кликнуть на элемент "priority"
      Когда подождать появления элемента "High" в течение 2 секунд
      Если кликнуть на элемент "High"
      Если очистить поле "email"
      Когда ввести в поле "email" значение "test@bk.ru"
      Если кликнуть на элемент "submitTicketBtn"

      И переход на страницу "TicketPage"
      Когда проскроллить страницу до элемента "AttachFileBtn"
      Если кликнуть на элемент "AttachFileBtn"
      Когда проскроллить страницу до элемента "BrowseLabel"
      Если вложить в элемент "BrowseInput" файл "C:\Users\79375\OneDrive\Документы\test.txt"
      Если кликнуть на элемент "UpdateThisTicket"

